# converter

A simple currency converter app utilising the [ECB rates](https://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml).

## Requirements

- [x] Fetches rates from the endpoint every 30s (even though the endpoint is updated only daily).
- [x] Supports EUR, GBP and USD.
- [x] The convert screen shows only two currencies at a time. The user can switch between currencies using an endless carousel controller. The conversion is calculated on user input. Also, when the user switches currencies.
- [x] The current exchange rate is displayed.
- [x] Conversion is triggered by clicking "Convert".
- [x] The user starts with 100 units of each currency. The app tracks the transaction and notifies the user when they have insufficient funds for a transaction.

### Notes

- Could've added tests tbh.
- `NSXMLParser` has built-in rudimentary networking capabilites, which is pretty cool.
- I forgot how funky the Objective-C syntax is.
