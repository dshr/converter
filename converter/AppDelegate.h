//
//  AppDelegate.h
//  converter
//
//  Created by Desislav Hristov on 21/07/2017.
//  Copyright © 2017 Desislav Hristov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CNVWallet.h"
#import "CNVConverterService.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

