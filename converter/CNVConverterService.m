//
//  CNVConverterService.m
//  converter
//
//  Created by Desislav Hristov on 25/07/2017.
//  Copyright © 2017 Desislav Hristov. All rights reserved.
//

#import "CNVConverterService.h"

@interface CNVConverterService ()

@property NSMutableDictionary<NSString*, NSNumber*>* rates;

@property NSXMLParser *parser;
@property NSArray<NSString *>* currencies;

@end

@implementation CNVConverterService

#pragma mark Singleton Methods

+ (id)sharedInstance {
  
  static CNVConverterService *sharedInstance = nil;
  static dispatch_once_t onceToken;
  dispatch_once(&onceToken, ^{
    
    sharedInstance = [[self alloc] init];
  });
  return sharedInstance;
}

# pragma mark Lifecycle

- (id)init {
  
  if (self = [super init]) {
    
    _currencies = [[NSArray<NSString *> alloc] initWithObjects: @"USD", @"GBP", nil];
    
    _rates = [[NSMutableDictionary<NSString*, NSNumber*> alloc] init];
    [_rates setObject:[NSNumber numberWithDouble:1.0f] forKey:@"EUR"];
    
    _ratesLoaded = false;
    
    [self fetchRates];
  }
  return self;
}

#pragma mark Public

- (double)rateOfCurrency:(CNVCurrency *)sourceSurrency toCurrency:(CNVCurrency *)targetCurrency {
  
  double sourceRate = [[_rates valueForKey: sourceSurrency.currency] doubleValue];
  double targetRate = [[_rates valueForKey: targetCurrency.currency] doubleValue];
  return (1.0f / sourceRate) * targetRate;
}

- (double)amount:(double)amount ofCurrency:(CNVCurrency *)sourceSurrency inCurrency:(CNVCurrency *)targetCurrency {
  
  return amount * [self rateOfCurrency:sourceSurrency toCurrency:targetCurrency];
}

#pragma mark Private

- (void)fetchRates {
  
  _parser = [[NSXMLParser alloc] initWithContentsOfURL:[NSURL URLWithString:@"https://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml"]];
  _parser.delegate = self;
  [_parser parse];
  NSError *error = [_parser parserError];
  if (error) {
    
    _ratesLoaded = false;
    [[NSNotificationCenter defaultCenter] postNotificationName:@"RatesUpdated" object:self];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
      
      [self fetchRates];
    });
    NSLog(@"Error %@", error);
  }
}

#pragma mark NSXMLParserDelegate

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict {
  
  if ([_currencies containsObject: [attributeDict objectForKey:@"currency"]]) {
    
    NSNumber* amount = [attributeDict objectForKey:@"rate"];
    NSString* currency = [attributeDict objectForKey:@"currency"];
    [_rates setObject:amount forKey:currency];
  }
}

- (void) parserDidEndDocument:(NSXMLParser *)parser {
  
  _ratesLoaded = true;
  [[NSNotificationCenter defaultCenter] postNotificationName:@"RatesUpdated" object:self];
  dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(30 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
    
    [self fetchRates];
  });
}

@end
