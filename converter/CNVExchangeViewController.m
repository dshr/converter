//
//  CNVExchangeViewController.m
//  converter
//
//  Created by Desislav Hristov on 22/07/2017.
//  Copyright © 2017 Desislav Hristov. All rights reserved.
//

#import "CNVExchangeViewController.h"

@interface CNVExchangeViewController ()

@property (weak, nonatomic) IBOutlet UIButton *convertButton;
@property (weak, nonatomic) IBOutlet UILabel *rateLabel;

@property CNVConverterService* converter;

@property UIPageViewController* withdrawViewController;
@property UIPageViewController* depositViewController;

@property NSMutableArray<UIViewController *>* withdrawViewControllers;
@property NSMutableArray<UIViewController *>* depositViewControllers;

@property CNVExchangeEditViewController* activeCurrencyViewController;

- (NSMutableArray<UIViewController *>*)collectionForPageViewController:(UIPageViewController *)controller;
- (void)conversionRatesUpdated;
- (void)updateCanConvert;
- (void)updateRateDisplay;
- (void)runConversions;
- (void)updateRatesAvailable;

@end

@implementation CNVExchangeViewController

#pragma mark Lifecycle

- (id)initWithCoder:(NSCoder *)aDecoder {
  
  self = [super initWithCoder:aDecoder];
  
  if (self) {
    
    self.converter = [CNVConverterService sharedInstance];
    
    CNVWallet* wallet = [CNVWallet sharedInstance];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    self.withdrawViewControllers = [[NSMutableArray<UIViewController *> alloc] init];
    self.depositViewControllers = [[NSMutableArray<UIViewController *> alloc] init];
    
    for (CNVCurrency* currency in wallet.currencies) {
      
      CNVExchangeWithdrawViewController* withdrawController = [storyboard instantiateViewControllerWithIdentifier: @"CNVExchangeWithdrawViewController"];
      withdrawController.currency = currency;
      withdrawController.delegate = self;
      [self.withdrawViewControllers addObject: withdrawController];
      
      CNVExchangeEditViewController* depositController = [storyboard instantiateViewControllerWithIdentifier: @"CNVExchangeEditViewController"];
      depositController.currency = currency;
      depositController.delegate = self;
      [self.depositViewControllers addObject: depositController];
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(conversionRatesUpdated) name:@"RatesUpdated" object:nil];
  }
  return self;
}

- (void)viewDidLoad {
  
  [super viewDidLoad];
  
  self.withdrawViewController = [self childViewControllers][0];
  self.depositViewController = [self childViewControllers][1];
  
  self.withdrawViewController.dataSource = self;
  self.withdrawViewController.delegate = self;
  self.depositViewController.dataSource = self;
  self.depositViewController.delegate = self;
  
  [self.withdrawViewController setViewControllers:[NSArray arrayWithObjects: [self.withdrawViewControllers objectAtIndex:0], nil] direction:UIPageViewControllerNavigationDirectionForward animated:false completion:nil];
  
  [self.depositViewController setViewControllers:[NSArray arrayWithObjects: [self.depositViewControllers objectAtIndex:1], nil] direction:UIPageViewControllerNavigationDirectionForward animated:false completion:nil];
  
  self.activeCurrencyViewController = (CNVExchangeWithdrawViewController *) [self.withdrawViewControllers objectAtIndex:0];
  [self.activeCurrencyViewController.amountField becomeFirstResponder];
}

- (void)viewWillAppear:(BOOL)animated {
  
  [super viewWillAppear:animated];
  [self updateRatesAvailable];
  [self updateRateDisplay];
}

#pragma mark Public

- (IBAction)convert:(id)sender {
  
  CNVExchangeEditViewController* withdrawController = self.withdrawViewController.viewControllers[0];
  CNVExchangeEditViewController* depositController = self.depositViewController.viewControllers[0];
  double amount = withdrawController.amount;
  CNVWallet* wallet = [CNVWallet sharedInstance];
  [wallet convertAmount:amount fromCurrency:withdrawController.currency toCurrency:depositController.currency];
  [[self navigationController] popViewControllerAnimated:true];
}

#pragma mark Private

- (NSMutableArray<UIViewController *>*)collectionForPageViewController:(UIPageViewController *)controller {
  
  if (controller == self.withdrawViewController) {
    
    return self.withdrawViewControllers;
  } else {
    
    return self.depositViewControllers;
  }
}

- (void)conversionRatesUpdated {
  
  [self updateRateDisplay];
  [self updateRatesAvailable];
  [self runConversions];
  [self updateCanConvert];
}

- (void)updateCanConvert {
  
  CNVExchangeWithdrawViewController *withdrawController = self.withdrawViewController.viewControllers[0];
  CNVExchangeWithdrawViewController *depositController = self.depositViewController.viewControllers[0];
  if ([withdrawController.currency isEqual: depositController.currency] || !self.converter.ratesLoaded) {
    
    [self.convertButton setEnabled: false];
    return;
  }
  [self.convertButton setEnabled: [withdrawController canWithdraw]];
}

- (void)updateRateDisplay {
  
  if (self.converter.ratesLoaded) {
    
    CNVExchangeWithdrawViewController *withdrawController = self.withdrawViewController.viewControllers[0];
    CNVExchangeEditViewController *depositController = self.depositViewController.viewControllers[0];
    
    CNVCurrency* sourceCurrency = withdrawController.currency;
    CNVCurrency* targetCurrency = depositController.currency;
    
    double rate = [self.converter rateOfCurrency:sourceCurrency toCurrency:targetCurrency];
    self.rateLabel.text = [NSString stringWithFormat: @"%@1 = %@%.4f", sourceCurrency.currencySymbol, targetCurrency.currencySymbol, rate];
  } else {
    
    self.rateLabel.text = @"Couldn't fetch rates. Retrying..";
  }
}

- (void)runConversions {
  
  CNVExchangeWithdrawViewController *withdrawController = self.withdrawViewController.viewControllers[0];
  
  NSArray<UIViewController *>* otherControllers;
  if ([self.activeCurrencyViewController isEqual: withdrawController]) {
    
    otherControllers = self.depositViewControllers;
  } else {
    
    otherControllers = self.withdrawViewControllers;
  }
  
  double amount = self.activeCurrencyViewController.amount;
  for (UIViewController *otherController in otherControllers) {
    
    CNVCurrency* sourceCurrency = self.activeCurrencyViewController.currency;
    CNVCurrency* targetCurrency = [(CNVExchangeEditViewController *)otherController currency];
    double newAmount = [self.converter amount:amount ofCurrency:sourceCurrency inCurrency:targetCurrency];
    [(CNVExchangeEditViewController *)otherController setAmount: newAmount];
  }
}

- (void)updateRatesAvailable {
  
  [self.withdrawViewController.view setUserInteractionEnabled: self.converter.ratesLoaded];
  [self.depositViewController.view setUserInteractionEnabled: self.converter.ratesLoaded];
  if (!self.converter.ratesLoaded) {
    
    [self.activeCurrencyViewController.amountField resignFirstResponder];
    self.activeCurrencyViewController.amountField.text = @"";
  }
}

#pragma mark CNVExchangeEditViewControllerDelegate

- (void)exchangeEditController:(CNVExchangeEditViewController *)controller amountDidChange:(double)amount {
  
  self.activeCurrencyViewController = controller;
  CNVExchangeWithdrawViewController *withdrawController = self.withdrawViewController.viewControllers[0];
  NSArray<UIViewController *>* siblings;
  if ([controller isEqual: withdrawController]) {
    
    siblings = self.withdrawViewControllers;
  } else {
    
    siblings = self.depositViewControllers;
  }
  
  for (UIViewController *sibling in siblings) {
    
    if (sibling != controller) {
      
      [(CNVExchangeEditViewController *)sibling setAmount: amount];
    }
  }
  [self runConversions];
  [self updateCanConvert];
}

#pragma mark UIPageViewControllerDelegate

- (void)pageViewController:(UIPageViewController *)pageViewController didFinishAnimating:(BOOL)finished previousViewControllers:(NSArray<UIViewController *> *)previousViewControllers transitionCompleted:(BOOL)completed {
  
  if (finished && completed) {
    
    UIPageViewController *otherPageViewController;
    if ([pageViewController isEqual: self.withdrawViewController]) {
  
      otherPageViewController = self.depositViewController;
    } else {
  
      otherPageViewController = self.withdrawViewController;
    }
    CNVExchangeEditViewController* sourceController = (CNVExchangeEditViewController *)otherPageViewController.viewControllers[0];
    CNVExchangeEditViewController* targetController = (CNVExchangeEditViewController *)pageViewController.viewControllers[0];
    if (![sourceController.amountField isFirstResponder]) {
      
      self.activeCurrencyViewController = targetController;
      dispatch_async(dispatch_get_main_queue(), ^{
        
        [targetController.amountField becomeFirstResponder];
      });
    }
    [self updateCanConvert];
    [self updateRateDisplay];
  }
}

#pragma mark UIPageViewControllerDataSource

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController {
  
  NSArray<UIViewController *>* collection = [self collectionForPageViewController: pageViewController];
  NSUInteger index = [collection indexOfObject: viewController];
  NSInteger count = [collection count];
  
  NSInteger targetIndex = index - 1;
  if (targetIndex < 0) {
    
    targetIndex = count + targetIndex;
  }
  return collection[targetIndex];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController {
  
  NSArray<UIViewController *>* collection = [self collectionForPageViewController: pageViewController];
  NSUInteger index = [collection indexOfObject: viewController];
  NSInteger count = [collection count];
  
  NSInteger targetIndex = index + 1;
  if (targetIndex > (count - 1)) {
    
    targetIndex = targetIndex - count;
  }
  return collection[targetIndex];
}

- (NSInteger)presentationCountForPageViewController:(UIPageViewController *)pageViewController {
  
  return [[self collectionForPageViewController: pageViewController] count];
}

- (NSInteger)presentationIndexForPageViewController:(UIPageViewController *)pageViewController {
  
  if (pageViewController == self.withdrawViewController) {
    
    return 0;
  } else {
    
    return 1;
  }
}

@end
