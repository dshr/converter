//
//  CNVExchangeEditViewController.h
//  converter
//
//  Created by Desislav Hristov on 22/07/2017.
//  Copyright © 2017 Desislav Hristov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CNVCurrency.h"

@class CNVExchangeEditViewController;

@protocol CNVExchangeEditViewControllerDelegate <NSObject>

- (void)exchangeEditController:(CNVExchangeEditViewController *)controller amountDidChange:(double)amount;

@end

@interface CNVExchangeEditViewController : UIViewController <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *amountField;
@property CNVCurrency* currency;
@property double amount;
@property (weak) id<CNVExchangeEditViewControllerDelegate> delegate;

- (void)amountDidChange:(BOOL)notify;

@end
