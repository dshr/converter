//
//  CNVConverterService.h
//  converter
//
//  Created by Desislav Hristov on 25/07/2017.
//  Copyright © 2017 Desislav Hristov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CNVCurrency.h"

@interface CNVConverterService : NSObject <NSXMLParserDelegate>

@property BOOL ratesLoaded;

+ (id)sharedInstance;
- (double)rateOfCurrency:(CNVCurrency *)sourceSurrency toCurrency:(CNVCurrency *)targetCurrency;
- (double)amount:(double)amount ofCurrency:(CNVCurrency *)sourceSurrency inCurrency:(CNVCurrency *)targetCurrency;

@end
