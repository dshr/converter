//
//  main.m
//  converter
//
//  Created by Desislav Hristov on 21/07/2017.
//  Copyright © 2017 Desislav Hristov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
	@autoreleasepool {
	    return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
	}
}
