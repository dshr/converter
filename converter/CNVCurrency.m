//
//  CNVCurrency.m
//  converter
//
//  Created by Desislav Hristov on 24/07/2017.
//  Copyright © 2017 Desislav Hristov. All rights reserved.
//

#import "CNVCurrency.h"

@interface CNVCurrency ()

@end

@implementation CNVCurrency

@synthesize amount = _amount;
@synthesize currency = _currency;

# pragma mark Lifecycle

- (id)init {
  
  return [self initWithAmount:0.0f inCurrency:@"USD"];
}

- (id)initWithAmount:(double)amount inCurrency:(NSString*)currency {
  
  self = [super init];
  if (self) {
    
    _amount = amount;
    _currency = currency;
  }
  return self;
}

# pragma mark Public

- (NSString *)amountString {
  
  return [NSString stringWithFormat: @"%@%.2f", [self currencySymbol], self.amount];
}

- (NSString *)currencySymbol {
  
  if ([_currency isEqual: @"GBP"]) {
    
    return @"💷";
  } else if ([_currency isEqual: @"EUR"]) {
    
    return @"💶";
  } else if ([_currency isEqual: @"USD"]) {
    
    return @"💵";
  }
  return @"#";
}

- (void)depositAmount:(double)amount {
  
  _amount += amount;
}

- (void)withdrawAmount:(double)amount {
  
  _amount -= amount;
}

@end
