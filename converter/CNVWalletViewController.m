//
//  CNVWalletViewController.m
//  converter
//
//  Created by Desislav Hristov on 22/07/2017.
//  Copyright © 2017 Desislav Hristov. All rights reserved.
//

#import "CNVWalletViewController.h"

@interface CNVWalletViewController ()

@property NSMutableArray<UIViewController *>* currencyBalances;

@end

@implementation CNVWalletViewController

#pragma mark - Lifsecycle

- (id)initWithCoder:(NSCoder *)coder {
  
  self = [super initWithCoder:coder];
  
  if (self) {
    
    CNVWallet* wallet = [CNVWallet sharedInstance];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    self.currencyBalances = [[NSMutableArray<UIViewController *> alloc] init];
    for (CNVCurrency* currency in wallet.currencies) {
      
      CNVCurrencyBalanceViewController *currencyBalanceController = [storyboard instantiateViewControllerWithIdentifier: @"CNVWalletViewController"];
      currencyBalanceController.currency = currency;
      [self.currencyBalances addObject: currencyBalanceController];
    }
    
    self.dataSource = self;
    
    [self setViewControllers:[NSArray arrayWithObjects: [self.currencyBalances objectAtIndex:0], nil] direction:UIPageViewControllerNavigationDirectionForward animated:false completion:nil];
  }
  
  return self;
}

- (void)viewDidLoad {
  
  [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
  
  [super didReceiveMemoryWarning];
}

#pragma mark - UIPageViewControllerDataSource

- (UIViewController *)pageViewController: (UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController {
  
  NSUInteger index = [self.currencyBalances indexOfObject: viewController];
  NSInteger count = [self.currencyBalances count];
  
  NSInteger targetIndex = index - 1;
  if (targetIndex < 0) {
    
    targetIndex = count + targetIndex;
  }
  return self.currencyBalances[targetIndex];
}

- (UIViewController *)pageViewController: (UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController {
  
  NSUInteger index = [self.currencyBalances indexOfObject: viewController];
  NSInteger count = [self.currencyBalances count];
  
  NSInteger targetIndex = index + 1;
  if (targetIndex > (count - 1)) {
    
    targetIndex = targetIndex - count;
  }
  return self.currencyBalances[targetIndex];
}

- (NSInteger)presentationCountForPageViewController:(UIPageViewController *)pageViewController {
  
  return [self.currencyBalances count];
}

- (NSInteger)presentationIndexForPageViewController:(UIPageViewController *)pageViewController {
  
  return 0;
}

@end
