//
//  CNVExchangeEditViewController.m
//  converter
//
//  Created by Desislav Hristov on 22/07/2017.
//  Copyright © 2017 Desislav Hristov. All rights reserved.
//

#import "CNVExchangeEditViewController.h"

@interface CNVExchangeEditViewController ()

@property (weak, nonatomic) IBOutlet UILabel *currencyLabel;

@end

@implementation CNVExchangeEditViewController

@synthesize amount = _amount;

#pragma mark Lifecycle

- (void)viewDidLoad {
  
  [super viewDidLoad];
  self.amountField.delegate = self;
  self.currencyLabel.text = [self.currency currencySymbol];
}

- (void)viewWillAppear:(BOOL)animated {
  
  [super viewWillAppear:animated];
  if (_amount) {
    
    self.amountField.text = [NSString stringWithFormat: @"%.2f", _amount];
  } else {
    
    self.amountField.text = @"";
  }
}

#pragma mark Public

- (double)amount {
  
  return _amount;
}

- (void)setAmount:(double)amount {
  
  _amount = amount;
  if (_amount) {
    
    self.amountField.text = [NSString stringWithFormat: @"%.2f", _amount];
  } else {
    
    self.amountField.text = @"";
  }
  [self amountDidChange: false];
}

- (void)amountDidChange:(BOOL)notify {
  
  if (notify) {
    
    [self.delegate exchangeEditController:self amountDidChange: _amount];
  }
}

#pragma mark UITextFieldDelegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
  
  BOOL shouldChange = true;
  if (string.length) {
    
    NSRange matchRange = [textField.text rangeOfString:@"."];
    if (matchRange.length > 0) {
      
      NSUInteger decimalPlaces = textField.text.length - matchRange.location;
      shouldChange = (decimalPlaces <= 2);
    }
  }
  if (shouldChange) {
    
    _amount = [[textField.text stringByReplacingCharactersInRange:range withString:string] doubleValue];
    [self amountDidChange: true];
  }
  return shouldChange;
}

@end
