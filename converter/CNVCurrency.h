//
//  CNVCurrency.h
//  converter
//
//  Created by Desislav Hristov on 24/07/2017.
//  Copyright © 2017 Desislav Hristov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CNVCurrency : NSObject

@property (readonly) double amount;
@property (readonly) NSString* currency;

- (id)initWithAmount:(double)amount inCurrency:(NSString*)currency;
- (NSString *)amountString;
- (NSString *)currencySymbol;
- (void)depositAmount:(double)amount;
- (void)withdrawAmount:(double)amount;

@end
