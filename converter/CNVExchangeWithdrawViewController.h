//
//  CNVExchangeWithdrawViewController.h
//  converter
//
//  Created by Desislav Hristov on 24/07/2017.
//  Copyright © 2017 Desislav Hristov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CNVExchangeEditViewController.h"

@class CNVExchangeWithdrawViewController;

@interface CNVExchangeWithdrawViewController : CNVExchangeEditViewController

- (BOOL)canWithdraw;

@end
