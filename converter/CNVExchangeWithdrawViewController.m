//
//  CNVExchangeWithdrawViewController.m
//  converter
//
//  Created by Desislav Hristov on 24/07/2017.
//  Copyright © 2017 Desislav Hristov. All rights reserved.
//

#import "CNVExchangeWithdrawViewController.h"

@interface CNVExchangeWithdrawViewController ()

@property (weak, nonatomic) IBOutlet UILabel *balanceLabel;

@end

@implementation CNVExchangeWithdrawViewController

# pragma mark Lifecycle

- (void)viewWillAppear:(BOOL)animated {
  
  [super viewWillAppear:animated];
  self.balanceLabel.text = [NSString stringWithFormat: @"You have %@", [self.currency amountString]];
}

#pragma mark Public

- (BOOL)canWithdraw {
  
  return !(self.amount > self.currency.amount);
}

#pragma mark Private

- (void)amountDidChange:(BOOL)notify {
  
  [super amountDidChange:notify];
  if ([self canWithdraw]) {
    
    self.balanceLabel.textColor = [UIColor blackColor];
  } else {
    
    self.balanceLabel.textColor = [UIColor redColor];
  }
}

@end
