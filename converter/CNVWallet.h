//
//  CNVWallet.h
//  converter
//
//  Created by Desislav Hristov on 23/07/2017.
//  Copyright © 2017 Desislav Hristov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CNVCurrency.h"
#import "CNVConverterService.h"

@interface CNVWallet : NSObject

@property NSArray<CNVCurrency *>* currencies;

+ (id)sharedInstance;
- (void)convertAmount:(double)amount fromCurrency:(CNVCurrency *)sourceCurrency toCurrency:(CNVCurrency *)targetCurrency;

@end
