//
//  CNVWallet.m
//  converter
//
//  Created by Desislav Hristov on 23/07/2017.
//  Copyright © 2017 Desislav Hristov. All rights reserved.
//

#import "CNVWallet.h"

@interface CNVWallet ()

@end

@implementation CNVWallet

#pragma mark Singleton Methods

+ (id)sharedInstance {
  
  static CNVWallet *sharedInstance = nil;
  static dispatch_once_t onceToken;
  dispatch_once(&onceToken, ^{
    
    sharedInstance = [[self alloc] init];
  });
  return sharedInstance;
}

# pragma mark Lifecycle

- (id)init {
  
  if (self = [super init]) {
    
    CNVCurrency* gpb = [[CNVCurrency alloc] initWithAmount:100.0f inCurrency:@"GBP"];
    CNVCurrency* eur = [[CNVCurrency alloc] initWithAmount:100.0f inCurrency:@"EUR"];
    CNVCurrency* usd = [[CNVCurrency alloc] initWithAmount:100.0f inCurrency:@"USD"];
    
    self.currencies = [NSArray arrayWithObjects:gpb, eur, usd, nil];
  }
  return self;
}

#pragma mark Public

- (void)convertAmount:(double)amount fromCurrency:(CNVCurrency *)sourceCurrency toCurrency:(CNVCurrency *)targetCurrency {
  
  CNVConverterService* converter = [CNVConverterService sharedInstance];
  double newAmount = [converter amount:amount ofCurrency:sourceCurrency inCurrency:targetCurrency];
  [sourceCurrency withdrawAmount:amount];
  [targetCurrency depositAmount:newAmount];
}


@end
