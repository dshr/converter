//
//  CNVCurrencyBalanceViewController.h
//  converter
//
//  Created by Desislav Hristov on 22/07/2017.
//  Copyright © 2017 Desislav Hristov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CNVCurrency.h"

@interface CNVCurrencyBalanceViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *amountLabel;
@property CNVCurrency* currency;

@end
