//
//  CNVExchangeViewController.h
//  converter
//
//  Created by Desislav Hristov on 22/07/2017.
//  Copyright © 2017 Desislav Hristov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CNVExchangeWithdrawViewController.h"
#import "CNVExchangeEditViewController.h"
#import "CNVWallet.h"
#import "CNVConverterService.h"

@interface CNVExchangeViewController : UIViewController <UIPageViewControllerDataSource, UIPageViewControllerDelegate, CNVExchangeEditViewControllerDelegate>

@end
