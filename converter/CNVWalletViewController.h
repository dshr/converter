//
//  CNVWalletViewController.h
//  converter
//
//  Created by Desislav Hristov on 22/07/2017.
//  Copyright © 2017 Desislav Hristov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CNVCurrencyBalanceViewController.h"
#import "CNVWallet.h"

@interface CNVWalletViewController : UIPageViewController <UIPageViewControllerDataSource>

@end
