//
//  CNVCurrencyBalanceViewController.m
//  converter
//
//  Created by Desislav Hristov on 22/07/2017.
//  Copyright © 2017 Desislav Hristov. All rights reserved.
//

#import "CNVCurrencyBalanceViewController.h"

@interface CNVCurrencyBalanceViewController ()

@end

@implementation CNVCurrencyBalanceViewController

# pragma mark Lifecycle

- (void)viewWillAppear:(BOOL)animated {
  
  self.amountLabel.text = self.currency.amountString;
  [super viewWillAppear:animated];
}

@end
